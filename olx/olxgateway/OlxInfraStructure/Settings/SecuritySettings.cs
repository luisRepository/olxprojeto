﻿using Newtonsoft.Json;

namespace OlxInfraStructure.Settings
{
    public class SecuritySettings
    {
        #region Properties

        public string UsuarioServiceProvider { get; set; }

        #endregion

        #region Operations

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        #endregion
    }
}