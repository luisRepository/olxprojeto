﻿using Microsoft.AspNetCore.Mvc;
using OlxDomain.Seguranca;
using OlxServices.Seguranca.Interfaces;
using Ultimate.Core.Infrastructure.Messaging;

namespace OlxApplication.Controllers.Seguranca
{
    [Route("api/seguranca/[controller]")]
    [ApiController]
    public class UsuarioController: ControllerBase
    {
        private readonly IUsuarioService usuarioService;

        #region Constructor

        public UsuarioController(IUsuarioService usuarioService)
        {
            this.usuarioService = usuarioService;
        }

        #endregion

        #region Operations

        [HttpPost]
        public IActionResult Incluir([FromBody] UsuarioDto usuarioDto)
        {
            Response<UsuarioDto> usuarioServiceResponse = usuarioService.Incluir(usuarioDto);

            return StatusCode((int)usuarioServiceResponse.ResponseCode, usuarioServiceResponse.Item);
        }

        #endregion
    }
}