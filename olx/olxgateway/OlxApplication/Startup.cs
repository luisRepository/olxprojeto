﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using OlxInfraStructure.Settings;
using OlxServices.Seguranca.Factories;
using OlxServices.Seguranca.Interfaces;
using System;
using System.Text;
using Ultimate.Core.Infrastructure.Communication.Http.Base;
using Ultimate.Core.Infrastructure.Communication.Http.Factories;
using Ultimate.Core.Infrastructure.Communication.Settings;
using Ultimate.Core.Infrastructure.Logging.Base;
using Ultimate.Core.Infrastructure.Logging.Factories;
using Ultimate.Core.Infrastructure.Logging.Settings;

namespace OlxApplication
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            LoggingSettings loggingSettings = JsonConvert.DeserializeObject<LoggingSettings>(Encoding.UTF8.GetString(Convert.FromBase64String(Environment.GetEnvironmentVariable("OLX_CORE_GATEWAY_LOGGINGSETTINGS"))));
            CommunicationSettings communicationSettings = JsonConvert.DeserializeObject<CommunicationSettings>(Encoding.UTF8.GetString(Convert.FromBase64String(Environment.GetEnvironmentVariable("OLX_CORE_GATEWAY_COMMUNICATIONSETTINGS"))));
            SecuritySettings securitySettings = JsonConvert.DeserializeObject<SecuritySettings>(Encoding.UTF8.GetString(Convert.FromBase64String(Environment.GetEnvironmentVariable("OLX_CORE_GATEWAY_SECURITYSETTINGS"))));

            // Seguranca service definitions
            string securityHost = Environment.GetEnvironmentVariable("SEGURANCA_SERVICE_HOST");
            string securityPort = Environment.GetEnvironmentVariable("SEGURANCA_SERVICE_PORT");
            string securityUri = $"http://{securityHost}:{securityPort}";

            ILoggingService loggingService = LoggingServiceFactory.Instance.Build(loggingSettings);
            IHttpService httpService = HttpServiceFactory.Instance.Build(communicationSettings, loggingService);

            IUsuarioService usuarioService = UsuarioServiceFactory.Instance.Build(securitySettings,httpService, securityUri);

            services.AddSingleton(usuarioService);
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
