﻿namespace OlxDomain.Seguranca
{
    public class UsuarioDto
    {
        #region Properties

        public string Login { get; set; }

        public string Password { get; set; }

        public string ConfirmarPassword { get; set; }

        #endregion
    }
}