﻿using OlxDomain.Seguranca;
using Ultimate.Core.Infrastructure.Messaging;

namespace OlxServices.Seguranca.Interfaces
{
    public interface IUsuarioService
    {
        #region Operations

        Response<UsuarioDto> Incluir(UsuarioDto usuarioDto);

        Response<UsuarioDto> Alterar(UsuarioDto usuarioDto);

        Response Excluir(string login);

        #endregion
    }
}