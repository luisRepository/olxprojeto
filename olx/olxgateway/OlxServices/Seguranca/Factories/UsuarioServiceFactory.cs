﻿using OlxInfraStructure.Settings;
using OlxServices.Seguranca.Interfaces;
using OlxServices.Seguranca.Services;
using System;
using Ultimate.Core.Infrastructure.Communication.Http.Base;

namespace OlxServices.Seguranca.Factories
{
    public class UsuarioServiceFactory
    {
        #region Attributes

        private static volatile UsuarioServiceFactory instance;

        #endregion

        #region Constructors

        private UsuarioServiceFactory()
        {
        }

        #endregion

        #region Properties

        public static UsuarioServiceFactory Instance
        {
            get { return instance ?? (instance = new UsuarioServiceFactory()); }
        }

        #endregion

        #region Operations

        public IUsuarioService Build(SecuritySettings securitySettings, IHttpService httpService, string securityUri)
        {
            switch (securitySettings.UsuarioServiceProvider)
            {
                case "Default":
                    return new UsuarioService(httpService, securityUri);

                default:
                    throw new ArgumentException("Provider de serviço inválido!");
            }
        }

        #endregion
    }
}