﻿using Newtonsoft.Json;
using OlxDomain.Seguranca;
using OlxServices.Seguranca.Interfaces;
using System;
using System.Net.Mime;
using Ultimate.Core.Infrastructure.Communication.Http.Base;
using Ultimate.Core.Infrastructure.Constants;
using Ultimate.Core.Infrastructure.Messaging;

namespace OlxServices.Seguranca.Services
{
    public class UsuarioService : IUsuarioService
    {
        #region Attributes

        private readonly IHttpService httpService;
        private readonly string securityUri;

        #endregion

        #region Constructor

        public UsuarioService(IHttpService httpService, string securityUri)
        {
            this.httpService = httpService;
            this.securityUri = securityUri;
        }

        #endregion

        #region Operations

        public Response<UsuarioDto> Incluir(UsuarioDto usuarioDto)
        {
            try
            {
                Response<string> httpServiceResponse = httpService.Post($"{securityUri}/api/seguranca/usuario", usuarioDto.ToString(), MediaTypeNames.Application.Json);

                if (!httpServiceResponse.Succeeded)
                {

                    return new Response<UsuarioDto>(httpServiceResponse.ResponseCode, false, httpServiceResponse.Message);
                }

                return new Response<UsuarioDto>(ResponseCode.OK, true, "OK", JsonConvert.DeserializeObject<UsuarioDto>(httpServiceResponse.Item));
            }
            catch (Exception exception)
            {
                return new Response<UsuarioDto>(ResponseCode.InternalServerError, false, MessageConst.EXCEPTIONMESSAGE);
            }
        }

        public Response<UsuarioDto> Alterar(UsuarioDto usuarioDto)
        {
            throw new NotImplementedException();
        }

        public Response Excluir(string login)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}